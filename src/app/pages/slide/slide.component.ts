import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Painting } from 'src/app/widgets/constants/data.constants';
import { SharedService } from 'src/app/widgets/shared.service';
import { filter } from 'rxjs/operators';
import { TranslateDirection } from 'src/app/widgets/slide-controller/slide-controller.component';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnInit {
  painting!: Painting | undefined;
  size = window.innerWidth <= 700 ? 'small' : 'large';
  animate = false;
  fadeInLeft = true;
  translateDirection: false | TranslateDirection = false;

  constructor(
    private sharedService: SharedService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(async () => {
        this.animate = false;
        await new Promise((resolve) => setTimeout(resolve, 100));
        this.animate = true;
      });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.painting = this.getCurrentPainting(params.painting);
      this.fadeInLeft =
        this.sharedService.currentSlideId > this.sharedService.previousSlideId;
    });
  }

  async buttonControlClicked(event: TranslateDirection) {
    this.translateDirection = event;
    await new Promise((resolve) => setTimeout(resolve, 200));
    this.animate = false;
    this.translateDirection = false;
  }

  getCurrentPainting(paintingLink: string): Painting | undefined {
    const currentPainting = this.sharedService.getCurrentPainting(paintingLink);
    if (!currentPainting) {
      const message = `page path: ${paintingLink} not found`;
      this.sharedService.setErrorMessage(message);
      this.router.navigateByUrl('/404');
      return;
    }
    return currentPainting;
  }

  getImageSource(type: 'hero' | 'artist') {
    const path = `../../../assets/images/paintings/${this.painting?.title}`;
    if (type === 'artist') {
      return `${path}/artist.jpg`;
    }
    return `${path}/hero-${this.size}.jpg`;
  }

  openPhotoViewer() {
    this.sharedService.togglePhotoViewer(true);
  }
}
