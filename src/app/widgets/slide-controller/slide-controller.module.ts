import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlideControllerComponent } from './slide-controller.component';
import { PhotoViewerModule } from '../photo-viewer/photo-viewer.module';

@NgModule({
  declarations: [SlideControllerComponent],
  imports: [CommonModule],
  exports: [SlideControllerComponent],
})
export class SlideControllerModule {}
