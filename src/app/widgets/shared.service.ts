import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Painting, paintings } from './constants/data.constants';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  paintings: Painting[] = paintings;
  currentSlideId = -1;
  previousSlideId = -1;
  currentSlide!: Painting | undefined;
  previousButtonClickable = false;
  nextButtonClickable = false;
  private photoViewer = new BehaviorSubject(false);
  photoViewerEvent = this.photoViewer.asObservable();
  private errorMessageSource = new BehaviorSubject('Page Not Found');
  currentErrorMessage = this.errorMessageSource.asObservable();

  constructor(private router: Router) {}

  getAsLink(paintingName: string) {
    return paintingName.split(' ').join('-').toLowerCase();
  }

  getCurrentPainting(paintingLink: string): Painting | undefined {
    this.currentSlide = this.paintings.find(
      (painting: Painting) => this.getAsLink(painting.title) === paintingLink
    );
    this.currentSlideId = this.currentSlide?.id ?? -1;
    this.setNextButton();
    this.setPreviousButton();
    return this.currentSlide;
  }

  setNextButton() {
    this.nextButtonClickable = this.currentSlideId < this.paintings.length - 1;
  }

  goToNextSlide() {
    if (this.nextButtonClickable) {
      const currentSlide = this.paintings[this.currentSlideId + 1];
      const currentLink = this.getAsLink(currentSlide.title);
      this.previousSlideId = this.currentSlideId - 1;
      this.router.navigateByUrl(`/slide/${currentLink}`);
    }
  }

  setPreviousButton() {
    this.previousButtonClickable = this.currentSlideId > 0;
  }

  goToPreviousSlide() {
    if (this.previousButtonClickable) {
      const currentSlide = this.paintings[this.currentSlideId - 1];
      const currentLink = this.getAsLink(currentSlide.title);
      this.previousSlideId = this.currentSlideId + 1;
      this.router.navigateByUrl(`/slide/${currentLink}`);
    }
  }

  togglePhotoViewer(open: boolean) {
    this.photoViewer.next(open);
  }

  setErrorMessage(message: string) {
    this.errorMessageSource.next(message);
  }
}
