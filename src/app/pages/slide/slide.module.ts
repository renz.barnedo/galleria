import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlideRoutingModule } from './slide-routing.module';
import { SlideComponent } from './slide.component';
import { SlideControllerModule } from 'src/app/widgets/slide-controller/slide-controller.module';
import { PhotoViewerModule } from 'src/app/widgets/photo-viewer/photo-viewer.module';

@NgModule({
  declarations: [SlideComponent],
  imports: [
    CommonModule,
    SlideRoutingModule,
    SlideControllerModule,
    PhotoViewerModule,
  ],
})
export class SlideModule {}
