import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Painting } from 'src/app/widgets/constants/data.constants';
import { SharedService } from 'src/app/widgets/shared.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
  paintings: Painting[] = [];
  @ViewChild('gallery') gallery!: ElementRef;
  show = false;

  constructor(private sharedService: SharedService, private router: Router) {}

  async ngOnInit() {
    this.setPaintings();
    await new Promise((resolve) => setTimeout(resolve, 300));
    this.show = true;
  }

  setPaintings() {
    this.paintings = this.sharedService.paintings;
  }

  getAsLink(paintingName: string) {
    return this.sharedService.getAsLink(paintingName);
  }

  async moveToSlide(painting: Painting) {
    this.show = false;
    await new Promise((resolve) => setTimeout(resolve, 500));
    const link = this.getAsLink(painting.title);
    this.router.navigateByUrl(`/slide/${link}`);
  }
}
