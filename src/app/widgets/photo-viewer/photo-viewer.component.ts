import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-photo-viewer',
  templateUrl: './photo-viewer.component.html',
  styleUrls: ['./photo-viewer.component.scss'],
})
export class PhotoViewerComponent implements OnInit {
  show = false;
  subscription!: Subscription;
  imageSource: string | undefined = '';

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.subscription = this.sharedService.photoViewerEvent.subscribe(
      (show: boolean) => (this.show = show)
    );
  }

  ngDoCheck() {
    this.imageSource = this.sharedService.currentSlide?.title;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closePhotoViewer() {
    this.sharedService.togglePhotoViewer(false);
  }
}
