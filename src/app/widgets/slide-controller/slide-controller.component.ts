import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { interval, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { SharedService } from '../shared.service';

export type TranslateDirection = 'previous' | 'next';

@Component({
  selector: 'app-slide-controller',
  templateUrl: './slide-controller.component.html',
  styleUrls: ['./slide-controller.component.scss'],
})
export class SlideControllerComponent implements OnInit {
  @Input() title = '';
  @Input() artist = '';
  @Input() id = -1;
  previousButtonClickable = this.sharedService.previousButtonClickable;
  nextButtonClickable = this.sharedService.nextButtonClickable;
  @Output() controlClicked = new EventEmitter<TranslateDirection>();
  showAnimation = true;
  timerInterval!: Subscription;
  pause = false;
  counter = 15;
  delay = 15;
  paintings = this.sharedService.paintings;
  screenWidth = window.innerWidth > 1440 ? 1440 : window.innerWidth;

  constructor(private sharedService: SharedService, private router: Router) {
    router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(async () => {
        this.setButtonsClickability();
        this.showAnimation = false;
        await new Promise((resolve) => setTimeout(resolve, 100));
        this.showAnimation = true;
      });
  }

  ngOnInit(): void {
    this.startCounter();
  }

  ngOnDestroy() {
    this.stopCounter();
  }

  resumeCounter() {
    this.pause = false;
    this.delay = this.counter;
    this.setCounterInterval();
  }

  pauseCounter() {
    this.pause = true;
    this.stopCounter();
  }

  stopCounter() {
    this.timerInterval.unsubscribe();
  }

  startCounter() {
    if (this.pause) {
      return;
    }
    this.delay = 15;
    this.counter = this.delay;
    this.setCounterInterval();
  }

  setCounterInterval() {
    this.timerInterval = interval(1000).subscribe((counter: number) => {
      counter++;
      this.counter--;
      const currentSlideId = this.sharedService.currentSlideId;
      const previousSlideId = this.sharedService.previousSlideId;
      if (counter % this.delay === 0) {
        const isLastSlide = currentSlideId === this.paintings.length - 1;
        const backwards = currentSlideId < previousSlideId;
        const isFirstSlide = currentSlideId == 0;
        const forwards = currentSlideId > previousSlideId;
        if (isLastSlide || (backwards && !isFirstSlide)) {
          this.goToPreviousSlide();
        } else if (isFirstSlide || (forwards && !isLastSlide)) {
          this.goToNextSlide();
        }
      }
    });
  }

  setButtonsClickability() {
    this.nextButtonClickable = this.sharedService.nextButtonClickable;
    this.previousButtonClickable = this.sharedService.previousButtonClickable;
  }

  async goToNextSlide() {
    if (this.nextButtonClickable) {
      this.controlClicked.next('next');
      this.stopCounter();
      await new Promise((resolve) => setTimeout(resolve, 500));
      this.sharedService.goToNextSlide();
      this.startCounter();
    }
  }

  async goToPreviousSlide() {
    if (this.previousButtonClickable) {
      this.controlClicked.next('previous');
      this.stopCounter();
      await new Promise((resolve) => setTimeout(resolve, 500));
      this.sharedService.goToPreviousSlide();
      this.startCounter();
    }
  }

  get scaleX() {
    const progress = (1 / this.paintings.length) * (this.id + 1);
    const scaleX = `scaleX(${progress})`;
    return scaleX;
  }

  getMarkerLeft(index: number) {
    const progress = (1 / this.paintings.length) * (index + 1);
    const translateX = progress * this.screenWidth - 5;
    return `${translateX}px`;
  }

  goToPainting(paintingName: string) {
    const path = this.sharedService.getAsLink(paintingName);
    this.router.navigateByUrl(`slide/${path}`);
    this.stopCounter();
    this.startCounter();
  }
}
